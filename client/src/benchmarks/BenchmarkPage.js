import {
  prepareDescriptor,
  prepareDescriptorDoubleSolarSystem,
} from "./benchmarks/calculateNextAccelerations";
import BrowserBenchmark from "./BrowserBenchmark";

import * as benchNextPosition from "./benchmarks/benchNextPosition";

export default function BenchmarkPage() {
  /**
   * @type {BenchmarkDescriptorType[]}
   */
  const benchmarks = [];

  // benchmarks.push(prepareDescriptor(40));
  //  benchmarks.push(prepareDescriptorDoubleSolarSystem(40));
  benchmarks.push(benchNextPosition.prepareDescriptor(40));
  // benchmarks.push(benchNextPosition.prepareDescriptorDoubleSolarSystem(40));
  /**
   *
   * @type {BenchmarkDescriptorType}
   */
  const simpleFunctionDescriptor = {
    description: "test",
    callback: (value) => {
      const v = Number.parseInt(value);
      const x = Math.pow(Math.random() * (Math.pow(v, v) * Math.cos(v) + 1), 2);
      console.log(x);
      return x;
    },
    argumentsFactory: (index, r) => 1 + r + index * Math.random(),
    cleanup: () => {},
    count: 40,
  };
  benchmarks.push(simpleFunctionDescriptor);
  return <BrowserBenchmark benchmarks={benchmarks} />;
}
