import updatePositions from "../../components/UpdatePositions";

import {
  initialPositionTF,
  initialVelocityTF,
  masses,
} from "../../physics/physics";
import tf from "@tensorflow/tfjs";

/**
 * prepare a benchmark harness for calculateNextAccelerations
 *
 * @param {number} optionalRepeat
 * @return {BenchmarkDescriptorType}
 */
export function prepareDescriptor(optionalRepeat = 40) {
  const dt = 0.1;

  return {
    description: "11 CelestialBody: updatePositions",
    callback: updatePositions,
    argumentsFactory: (index, result) => {
      // positionRef,
      //     velocityRef,
      //     deltaTimeTensor,
      //     numberOfBodies,
      //     masses

      if (index === 0) {
        return [
          { current: initialPositionTF.clone() },
          { current: initialVelocityTF.clone() },
          dt,
          masses.length,
          masses,
        ];
      }
      return [
        { current: result.newPositionTf },
        { current: result.newVelocityTf },
        dt,
        masses.length,
        masses,
      ];
    },
    cleanup: (previousArguments, result) => {
      result.newAccelerationTf.dispose();
    },
    count: optionalRepeat,
  };
}

/**
 * prepare a benchmark harness for calculateNextAccelerations
 *
 * @param {number} optionalRepeat
 * @return {BenchmarkDescriptorType}
 */
export function prepareDescriptorDoubleSolarSystem(optionalRepeat = 40) {
  const doubleMasses = Array.from(masses).concat(masses);
  const dt = 0.1;

  const doubleInitialPositionTF = initialPositionTF
    .clone()
    .concat(initialPositionTF)
    .clone();
  const doubleInitialVelocityTF = initialVelocityTF
    .clone()
    .concat(initialPositionTF)
    .clone();

  return {
    description: "22 Celestial Body: updatePositions",
    callback: updatePositions,
    argumentsFactory: (index, result) => {
      if (index === 0) {
        return [
          { current: doubleInitialPositionTF },
          { current: doubleInitialVelocityTF },
          dt,
          masses.length,
          masses,
        ];
      }
      return [
        { current: result.newPositionTf },
        { current: result.newVelocityTf },
        dt,
        doubleMasses.length,
        doubleMasses,
      ];
    },
    cleanup: (previousArguments, result) => {
      result.newAccelerationTf.dispose();
    },
    count: optionalRepeat,
  };
}
