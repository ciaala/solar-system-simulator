import { calculateNextAccelerations } from "../../physics/physics";
import { initialPositionTF, masses } from "../../physics/physics";
import tf from "@tensorflow/tfjs";

/**
 * prepare a benchmark harness for calculateNextAccelerations
 *
 * @param {number} optionalRepeat
 * @return {BenchmarkDescriptorType}
 */
export function prepareDescriptor(optionalRepeat = 40) {
  return {
    description: "11 CelestialBody: calculateNextAccelerations",
    callback: calculateNextAccelerations,
    argumentsFactory: (index, result) => {
      if (index === 0) {
        return [initialPositionTF.clone(), masses.length, masses];
      }
      return [result, masses.length, masses];
    },
    cleanup: ([previousArguments, length, masses]) => {
      previousArguments.dispose();
    },
    count: optionalRepeat,
  };
}

/**
 * prepare a benchmark harness for calculateNextAccelerations
 *
 * @param {number} optionalRepeat
 * @return {BenchmarkDescriptorType}
 */
export function prepareDescriptorDoubleSolarSystem(optionalRepeat = 40) {
  const doubleMasses = Array.from(masses).concat(masses);
  const doubleInitialPositionTF = initialPositionTF
    .clone()
    .concat(initialPositionTF)
    .clone();

  return {
    description: "22 x Body: calculateNextAccelerations",
    callback: calculateNextAccelerations,
    argumentsFactory: (index, result) => {
      if (index === 0) {
        return [doubleInitialPositionTF, doubleMasses.length, doubleMasses];
      }
      return [result, masses.length, masses];
    },
    cleanup: ([previousArguments, length, masses]) => {
      previousArguments.dispose();
    },
    count: optionalRepeat,
  };
}
