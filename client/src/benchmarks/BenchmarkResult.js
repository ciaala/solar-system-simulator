import styles from "./BrowserBenchmark.module.css";
import Plot from "./Plot";

function calculateStdDev(length, results, mean) {
  return Math.sqrt(
    (1 / length) *
      results.reduce((acc, value) => acc + Math.pow(value - mean, 2), 0)
  );
}

function calculateSum(results) {
  return results.reduce((acc, value) => acc + value, 0);
}

/**
 *
 * @param {BenchmarkDescriptorType} benchmark
 * @return {JSX.Element}
 * @constructor
 */
export default function BenchmarkResult({ benchmark }) {
  const results = Array.from(new Float32Array(benchmark.results).sort());

  const sums = calculateSum(results);
  const length = results.length;
  const mean = sums / length;

  const median = results[length / 2];
  const min = results[0];
  const max = results[length - 1];
  const stdDev = calculateStdDev(length, results, mean);
  const results95 = Array.from(results).slice(1, length - 1);
  const mean95 = calculateSum(results95) / results95.length;
  const stdDev95 = calculateStdDev(results95.length, results95, mean95);
  const values = [benchmark.count, mean, median, min, max, stdDev, mean95, stdDev95];
  const formattedValues = values.map((value) => Number.isInteger(value) ? value : value.toFixed(4));
  return (
    <tr className={styles.BrowserBenchmarkResult}>
      <td>{benchmark.description}</td>
      {formattedValues.map((value, index) => (
        <td key={index}>{value}</td>
      ))}
      <td>
        <Plot values={benchmark.results} min={min} max={max} mean={mean} />
      </td>
      <td>
        <Plot values={results} min={min} max={max} mean={mean} />
      </td>
    </tr>
  );
}
