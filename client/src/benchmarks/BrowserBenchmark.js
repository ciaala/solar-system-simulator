import { useEffect, useState } from "react";
import BenchmarkResult from "./BenchmarkResult";
import benchmarkRunner from "./BenchmarkRunner";
import styles from "./BrowserBenchmark.module.css";

/**
 * @typedef {function} ArgumentsFactory
 * @param {number} index
 * @param {*} previousResult
 */

/**
 * @typedef {Object} BenchmarkDescriptorType
 * @property {function(*)} callback
 * @property {function(*,*)} cleanup
 * @property {ArgumentsFactory} argumentsFactory
 * @property {number} count
 * @property {string} description
 * @property {Float32Array} results
 */

const defaults = {
  // repeat at many time as 40;
  count: 40,
};

/**
 *
 * @param {BenchmarkDescriptorType[]} benchmarks
 * @return {JSX.Element}
 * @constructor
 */
function BrowserBenchmark({ benchmarks }) {
  const [results, setResults] = useState();
  const [errors, setErrors] = useState();

  useEffect(() => {
    const [allResults, allErrors] = benchmarkRunner(benchmarks);
    setResults(allResults);
    setErrors(allErrors);
  }, []);

  const allResults = results ? (
    results.map((benchmark, index) => (
      <BenchmarkResult key={"result" + index} benchmark={benchmark} />
    ))
  ) : (
    <tr>
      <td>Running...</td>
    </tr>
  );
  const allErrors = errors
    ? errors.map((error, index) => (
        <div key={"error-" + index}>
          {error.description}
          {error.iteration}
          {error.e}
        </div>
      ))
    : null;
  return (
    <div>
      <h1>Benchmarks</h1>
      <div className={styles.BenchmarkResults}>
        <table>
          <thead>
            <tr>
              <th>Description</th>
              <th>Repetition</th>
              <th>Mean</th>
              <th>Median</th>
              <th>Min</th>
              <th>Max</th>
              <th>StdDev</th>
              <th>Mean95</th>
              <th>StdDev95</th>
              <th>Plot</th>
              <th>Sorted Plot</th>
            </tr>
          </thead>
          <tbody>{allResults}</tbody>
        </table>
      </div>
      <div className={styles.BenchmarkErrors}>{allErrors}</div>
    </div>
  );
}

export default BrowserBenchmark;
