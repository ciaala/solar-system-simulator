import { useEffect, useRef } from "react";
import styles from "./BrowserBenchmark.module.css";

/**
 *
 * @param {Float32Array} values
 * @param {number} min
 * @param {number} max
 * @param {number} mean
 * @return {JSX.Element}
 * @constructor
 */
export default function Plot({ values, min, max, mean }) {
  const canvasRef = useRef(null);

  /**
   *
   * @param {canvas.context} context
   */
  function draw(context) {
    context.beginPath();
    context.strokeStyle = "#50AAAA";
    const y0 = context.canvas.height;
    //  context.moveTo(0, y0);
    const xInc = context.canvas.width / values.length;
    const yInc = context.canvas.height / (max - min);
    values.forEach((value, index) => {
      context.moveTo(index * xInc, y0);
      const yValue = y0 - (value - min) * yInc;
      context.lineTo(index * xInc, yValue);
    });
    context.stroke();
    context.closePath();
    context.beginPath();
    context.strokeStyle = "#FF8080";
    const yMeanScaled = y0 - mean * yInc;
    context.moveTo(0, yMeanScaled);
    context.lineTo(context.canvas.width, yMeanScaled);
    context.stroke();
    context.closePath();
    return;
  }

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    if (draw) {
      draw(context);
    } else {
      context.fillStyle = "#FF0000";
      context.fillRect(0, 0, context.canvas.width, context.canvas.height);
    }
  }, []);
  return (
    <div className={styles.BenchmarkPlot}>
      <div className={styles.PlotMinMax}>
        <span>{max.toFixed(1)}</span>
        <span>{mean.toFixed(1)}</span>
        <span>{min.toFixed(1)}</span>
      </div>
      <div className={styles.PlotCanvasWrapper}>
        <canvas ref={canvasRef} width={400} height={200} />
      </div>
    </div>
  );
}
