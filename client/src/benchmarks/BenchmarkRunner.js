/**
 *
 * @param {BenchmarkDescriptorType[]} benchmarks
 * @returns {[BenchmarkDescriptorType[], []]}
 * @constructor
 */
export default function benchmarkRunner(benchmarks) {
  const errors = [];
  console.log({ benchmarks });
  benchmarks.forEach((benchmark) => {
    console.log("Running ", benchmark);
    const count = benchmark.count || 10;
    const results = new Float32Array(count);
    // INIT
    let args = benchmark.argumentsFactory(0, undefined);

    for (let i = 0; i < count; i++) {
      let r;
      const start = performance.now();
      try {
        r = benchmark.callback.apply(null, args);
      } catch (e) {
        errors.push({
          description: benchmark.description,
          iterations: i,
          error: e,
        });
      }
      const end = performance.now();
      const oldArgs = args;
      args = benchmark.argumentsFactory(i, r);
      benchmark.cleanup(oldArgs, r);
      results.set([end - start], i);
    }
    benchmark.results = results;
  });
  return [benchmarks, errors];
}
