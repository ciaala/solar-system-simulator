import {Stats} from '@react-three/drei';
import {useRef, useState} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import styles from './App.css';

import {ConfiguredChat} from './ConfiguredChat';
import Menu from './routes/Menu';
import Space from './routes/Space';
import SolarSystem from './components/SolarSystem';
import Assemblies from './routes/Assemblies';
import Models from './routes/Models';
import {ServerWebSocketContextProvider} from './connection/useWebSocket';
import {SolarSystemContextProvider} from './solarsystem/SolarSystemContext';
import {BridgeContextWrapper} from './BridgeContextWrapper';

/**
 *
 * @return {JSX.Element}
 * @constructor
 */
function App() {
  const [time, setTime] = useState(0.05);
  const [zoom, setZoom] = useState(1);
  /**
   *
   */
  const [planetGravityField, setPlanetGravityField] = useState(1);
  const afterRenderActionList = useRef([]);
//  const contextBridge = useContextBridge(SolarSystemContext);
  const [celestialBodySelection, setCelestialBodySelection] = useState(
      {'spaceship': true, 'planet': true, 'star': true},
  );
  /**
   *
   * @type {React.MutableRefObject<number>}
   */
  const timeRef = useRef(0);
  // const solarSystem = useSolarSystem({
  //   dt: time,
  //   initialPlanets,
  //   initialPositionArray,
  //   initialVelocityArray,
  //   initialPositionTF,
  //   initialVelocityTF,
  //   masses,
  // });

  // /**
  //  *
  //  * @param {SpaceShipForm} shipForm
  //  *
  //  */
  // function createShip(shipForm) {
  //   afterRenderActionList.current.push(() => {
  //     const originalTime = time;
  //     setTime(0);
  //     solarSystem.addSpaceShip(shipForm);
  //     setTime(originalTime);
  //   });
  // }
  return (
      <div className={styles.App}>
        <Router>
          {/*<button*/}
          {/*    onClick={() =>*/}
          {/*        createShip({name: 'USS Dolphin', fuel: 1234, isp: 1234})*/}
          {/*    }*/}
          {/*>*/}
          {/*  Click !*/}
          {/*</button>*/}
          <Menu timeRef={timeRef}/>

          <Routes>
            {/*<Route exact={true} path={'/build'}*/}
            {/*       element={<Build onCreation={createShip}/>}/>*/}
            <Route exact={true} path="/models" element={
              <>
                <Models/>
                <Stats className={styles.Stats}/>
              </>}/>
            <Route exact={true} path="/assemblies" element={
              <>
                <Assemblies/>
                <Stats className={styles.Stats}/>
              </>}/>
            {/*<Route exact={true} path={'/benchmark'} element={<BenchmarkPage/>}/>*/}
            {/*<Route exact={true} path={'/play'} element={<Play/>}/>*/}

            <Route exact={true} path={''} element={<>


              {/*<SolarSystemContextProvider>*/}
              <Space
                  time={time}
                  setTime={setTime}
                  zoom={zoom}
                  setZoom={setZoom}
                  planetGravityField={planetGravityField}
                  setPlanetGravityField={setPlanetGravityField}
                  timeRef={timeRef}
                  celestialBodySelection={celestialBodySelection}
                  setCelestialBodySelection={setCelestialBodySelection}
              >
                <ServerWebSocketContextProvider serverUrl={'ws://localhost:3001'} userId="someId">

                  <SolarSystem
                      dt={time}
                      planetGravityField={planetGravityField}
                      timeRef={timeRef}

                      //{...solarSystem}
                      afterRenderActionList={afterRenderActionList}
                      celestialBodySelection={celestialBodySelection}
                  />
                </ServerWebSocketContextProvider>


              </Space>
              {/*</SolarSystemContextProvider>*/}


              <Stats className={styles.Stats}/>
            </>}/>
            <Route exact={true} path="/chat" element={<ConfiguredChat/>}/>
          </Routes>
        </Router>
      </div>
  );
}

export default App;
