import { Canvas } from "@react-three/fiber";
import { useState } from "react";
import { Vector3 } from "three";
import Axis from "../components/Axis";
import GLTF from "../components/GLTF";
import Plane from "../components/Plane";
import Renderer from "../components/Renderer";
import styles from "./Models.module.css";
import { Suspense } from "react";

const models = [
  "",
  "rings-space-ship/rings-space-ship",
  "ion-engine/ion-thruster-cluster-2x2",
  "ion-engine/ion-thruster-cluster-2x2-particle",
];
const origin = new Vector3();
const built = models.reduce(
  (acc, id, index) => (
    (acc[id] = id ? (
      <GLTF
        key={"model-" + index}
        model={"/resources/" + id + ".glb"}
        position={origin}
        rotation={origin}
      />
    ) : null),
    acc
  ),
  {}
);

function Models(props) {
  const [current, setCurrent] = useState("");

  return (
    <>
      <h1> Pick a model</h1>
      <select
        className={styles.ModelSelector}
        value={current}
        onChange={(event) => {
          console.log(event.target.value);
          setCurrent(event.target.value);
        }}
      >
        {models.map((model) => (
          <option key={"model" + model} value={model}>
            {model}
          </option>
        ))}
      </select>
      <Canvas
        className={styles.CanvasWrapper}
        concurrent
        camera={{ position: [10, -5, 10] }}
      >
        <Renderer>
          <Axis />
          {built[current]}
          <pointLight position={[10, 0, 0]} />
          <pointLight position={[0, 10, 0]} />
          <pointLight position={[0, 0, 10]} />
        </Renderer>
      </Canvas>
    </>
  );
}

export default Models;
