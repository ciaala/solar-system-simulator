import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import styles from "./Menu.module.css";
import { Link } from "react-router-dom";

function MenuClickable({ text, path }) {
  return (
    <Link className={styles.MenuLink} key={text} to={path}>
      {text}
    </Link>
  );
}

function Menu({ timeRef }) {
  const [mdate, setMdate] = useState("Starting");
  useEffect(() => {
    const uuid = setInterval(() => {
      if (!timeRef || !timeRef.current) {
        return;
      }
      let fixed = timeRef.current.toFixed(1);
      if (mdate !== fixed) {
        setMdate(fixed);
      }
    }, 330);
    return () => clearInterval(uuid);
  }, []);
  return (
    <div className={styles.Menu}>
      <MenuClickable text="Space" path="/" />
      {/*<MenuClickable text="Fleet" path="/fleet" />*/}
      {/*<MenuClickable text="Build" path="/build" />*/}
      {/*<MenuClickable text="Models" path="/models" />*/}
      <MenuClickable text="Assemblies" path="/assemblies" />
      {/*<MenuClickable text="Benchmark" path={"/benchmark"} />*/}
      {/*<MenuClickable text="Play" path={"/play"} />*/}
      <MenuClickable text='Chat' path='/chat'/>
      <span>Date {mdate}</span>
    </div>
  );
}

Menu.propTypes = {
  timeRef: PropTypes.object,
};

export default Menu;
