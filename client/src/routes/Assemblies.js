import { Canvas } from "@react-three/fiber";
import { useMemo, useState } from "react";
import { Vector3 } from "three";
import Axis from "../components/Axis";
import Planet from "../components/Planet";
import PlanetDetail from "../components/PlanetDetail";
import Renderer from "../components/Renderer";
import Trajectory from "../components/Trajectory";
import IonThrusterCluster2x2 from "../ship/engines/ion-thruster-cluster-2x2";
import SpaceShip from "../ship/SpaceShip";
import styles from "./Assemblies.module.css";


const origin = new Vector3();

const assembliesMap = {
  "": (
    <mesh>
      <sphereGeometry args={[1, 16, 16]} />
      <meshBasicMaterial color="white" wireframe={true} />
    </mesh>
  ),
  Spaceship: (
    <SpaceShip
      position={origin}
      orientation={new Vector3(0, -0.1, 0)}
      spaceShip={{ color: "red", name: "Uss Dolphin" }}
      planetGravityField={1}
      acceleration={origin}
      isEngineOn={true}
      dt={1}
      scale={0.5}
    />
  ),
  Ion: <IonThrusterCluster2x2 rotation={origin} position={origin} />,
  PlanetDetail: (
    <PlanetDetail
      position={origin}
      acceleration={origin}
      color={"purple"}
      velocity={new Vector3(1, 0, 0)}
      planet={{ name: "Planet-X", color: "purple", r: 3 }}
      planetGravityField={1.0}
    />
  ),
  Planet: <Planet color="purple" position={origin.toArray()} radius={1.0} />,
  Trajectory: (
    <Trajectory
      color="purple"
      points={[new Vector3(-1, -1, 0), origin, new Vector3(0.5, 1, 0)]}
    />
  ),
};

/**
 *
 * @param props
 * @return {JSX.Element}
 * @constructor
 */
function Assemblies(props) {
  const [current, setCurrent] = useState("");
  const assembliesOptions = useMemo(() => {
    return Object.keys(assembliesMap).map((assemblyName) => (
      <option key={"assembly" + assemblyName} value={assemblyName}>
        {assemblyName}
      </option>
    ));
  }, [assembliesMap]);
  return (
    <>
      <h1>Pick a model</h1>
      <select
        className={styles.AssemblySelector}
        value={current}
        onChange={(event) => {
          setCurrent(event.target.value);
        }}
      >
        {assembliesOptions}
      </select>
      <Canvas
        className={styles.CanvasWrapper}
        concurrent
        camera={{ position: [10, -5, 10] }}
      >
        <Renderer>
          <Axis />
          {assembliesMap[current]}
          <pointLight position={[10, 0, 0]} />
          <pointLight position={[0, 10, 0]} />
          <pointLight position={[0, 0, 10]} />
        </Renderer>
      </Canvas>
    </>
  );
}

export default Assemblies;
