import PropTypes from 'prop-types';
import {Canvas} from '@react-three/fiber';
import Axis from '../components/Axis';
import Plane from '../components/Plane';
import Renderer from '../components/Renderer';
import UI from '../configuration/UI';
import styles from './Space.module.css';
import {SolarSystemContext} from '../solarsystem/SolarSystemContext';
import {ServerWebSocketContext} from '../connection/useWebSocket';
import {useContextBridge} from '@react-three/drei';
import {BridgeContextWrapper} from '../BridgeContextWrapper';

/**
 *
 * @param time
 * @param setTime
 * @param zoom
 * @param setZoom
 * @param planetGravityField
 * @param setPlanetGravityField
 * @param children
 * @param celestialBodySelection
 * @param setCelestialBodySelection
 * @return {JSX.Element}
 * @constructor
 */
function Space({
                 time,
                 setTime,
                 zoom,
                 setZoom,
                 planetGravityField,
                 setPlanetGravityField,
                 children,
                 celestialBodySelection,
                 setCelestialBodySelection,
               }) {
  return (
      <>
        <UI
            time={time}
            setTime={setTime}
            zoom={zoom}
            setZoom={setZoom}
            planetGravityField={planetGravityField}
            setPlanetGravityField={setPlanetGravityField}
            celestialBodySelection={celestialBodySelection}
            setCelestialBodySelection={setCelestialBodySelection}
        />

        <Canvas
            className={styles.CanvasWrapper}
            concurrent
            camera={{position: [10, -5, 10], zoom, near: 0.0001}}
        >
          {/*<BridgeContextWrapper>*/}
            <Renderer zoom={zoom}>
              <Plane color="#808001"/>
              <Axis/>
              {children}
            </Renderer>
          {/*</BridgeContextWrapper>*/}
        </Canvas>
      </>
  );
}

Space.propTypes = {
  setPlanetGravityField: PropTypes.func,
  planetGravityField: PropTypes.number,
  setTime: PropTypes.func,
  time: PropTypes.number,
  setZoom: PropTypes.func,
  zoom: PropTypes.number,
  setCelestialBodySelection: PropTypes.func,
  celestialBodySelection: PropTypes.object.isRequired,
};

export default Space;
