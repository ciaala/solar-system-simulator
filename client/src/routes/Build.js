import * as PropTypes from "prop-types";
import { useState } from "react";
import styles from "./Build.module.css";

/**
 *
 * @param props
 * @constructor
 */
function TextInput(props) {
  return (
    <div className={styles.TextInput}>
      <label>{props.title}</label>
      <input
        placeholder={props.title}
        value={props.state[props.stateKey]}
        name={props.stateKey}
        onChange={props.onChange}
      />
    </div>
  );
}

TextInput.propTypes = {
  title: PropTypes.string.isRequired,
  stateKey: PropTypes.string.isRequired,
  state: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

/**
 *
 * @param props
 * @constructor
 */

function NumberInput(props) {
  return (
    <div className={styles.NumberInput}>
      <label>{props.title}</label>
      <input
        placeholder={props.title}
        value={props.state[props.stateKey]}
        name={props.stateKey}
        onChange={props.onChange}
        type="number"
      />
    </div>
  );
}

NumberInput.propTypes = {
  title: PropTypes.string.isRequired,
  stateKey: PropTypes.string.isRequired,
  state: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

function makeInput(title, stateKey, type) {
  return { title, stateKey, type };
}

const CreationForm = {
  elements: [
    makeInput("Name", "name", "text"),
    makeInput("Fuel", "fuel", "number"),
    makeInput("ISP", "isp", "number"),
  ],
  handleInputChange: function (event, state, setState) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    setState({
      ...state,
      [name]: value,
    });
  },
  handleSubmit: function (state) {
    console.log(state);
    if (this.elements.every((element) => state[element.stateKey])) {
      this.onCreation(state);
    }
  },
};

/**
 *
 * @param props
 * @constructor
 */
function Build(props) {
  const [state, setState] = useState({});

  function onChange(event) {
    return CreationForm.handleInputChange(event, state, setState);
  }

  CreationForm.onCreation = props.onCreation;
  const formElements = CreationForm.elements.map((element) => {
    if (element.type === "text") {
      return (
        <TextInput
          key={"form-" + element.stateKey}
          {...element}
          state={state}
          onChange={onChange}
        />
      );
    } else if (element.type === "number") {
      return (
        <NumberInput
          key={"form-" + element.stateKey}
          {...element}
          state={state}
          onChange={onChange}
        />
      );
    } else {
      console.log("Unknown type" + element.type);
      return null;
    }
  });
  return (
    <div className={styles.BuildForm}>
      <h1>Build Spaceship</h1>
      {formElements}
      <button onClick={() => CreationForm.handleSubmit(state)}>Create</button>
    </div>
  );
}

Build.propTypes = {
  onCreation: PropTypes.func.isRequired,
};
export default Build;
