import {useServerWebSocketContext} from '../connection/useWebSocket.js';
import {useEffect, useMemo, useRef, useState} from 'react';
import styles from './Chat.module.css';
import PropTypes from 'prop-types';

/**
 *
 * @param {string} userId
 * @return {JSX.Element}
 * @constructor
 */
function Chat({userId}) {
  const textRef = useRef();
  const [messages, setMessages] = useState([]);

  function receive(message) {
    if (message && message.type === 'chat') {
      setMessages(state => ([...state, message]));
    }
  }

  useEffect(() => {
    console.log({messages});
  }, [messages]);
  const {send, isConnected} = useServerWebSocketContext(receive);

  /** @type {Element[]} */
  const messageSpans = useMemo(
      () => messages.map((element, index) => <p key={index}>
        <span className={styles.MessageSource}>{element.userId}</span>
        <span className={styles.MessageBody}>{element.message}</span></p>)
      , [messages]);

  /**
   *
   * @param {string} line
   */
  function handleSendMessage(line) {
    console.log(line);
    if (line && line[0] === '/') {
      const tokens = line.split(' ');
      const type = tokens[0].substring(1);
      tokens.shift();
      const message = {
        type: 'command',
        command: {type, tokens},
        userId: userId
      };
      send(message);
    } else {
      const message = {type: 'chat', message: line, userId: userId};
      send(message);
    }

  }

  function handleConnectButton() {
    console.log('connect');
  }

  return <div className={styles.Chat}>

    <div className={styles.Messages}>
      {messageSpans}
    </div>
    <div className={styles.InputLine}>

      {isConnected ? <>
            <span>{userId}</span>
            <input type="text" ref={textRef} defaultValue={'/addShip 1 2 3'}/>
            <button onClick={() => handleSendMessage(textRef.current.value)}>Send
            </button>
          </>
          : <>
            <input type="text" disabled={true} defaultValue="not connected"/>
            <button onClick={handleConnectButton}>Connect</button>
          </>}
    </div>
  </div>
      ;
}

Chat.propTypes = {
  userId: PropTypes.string.isRequired,
};

export default Chat;




