import { useMemo } from "react";
import Plot from "../benchmarks/Plot";

const values = new Float32Array([10, 20, 30, 100, 200, 300, 1000, 2000, 3000]);
export default function Play() {
  return (
    <>
      <h1>Play</h1>
      <Plot values={values} max={3000} min={10} />
    </>
  );
}
