import { array } from "prop-types";
import { useCallback, useMemo, useRef, useState } from "react";
import updatePositions from "../components/UpdatePositions";
import updateTrails from "../components/UpdateTrails";
import * as tf from "@tensorflow/tfjs";

import { Vector3 } from "three";
import { KG_MASS_CONVERSION, rColor } from "./physics";

const CAPTURE_TRAJECTORY_EVERY_NTH_STEP = 10;
const ACCURACY_OF_STEP = 10;



function useSolarSystem({
  dt,
  initialPlanets,
  initialPositionArray,
  initialVelocityArray,
  initialPositionTF,
  initialVelocityTF,
  masses,
}) {
  const [planets, setPlanets] = useState(initialPlanets);
  const captureTrajectoryEveryNthStep = useMemo(() => {
    const capture = Math.ceil(
      CAPTURE_TRAJECTORY_EVERY_NTH_STEP / (ACCURACY_OF_STEP * dt)
    );
    return capture;
  }, [dt]);

  const nTimeSteps = useRef(1);
  const initialTrajectoryArray = initialPositionArray.map((initialPosition) =>
    Array(1).fill(new Vector3(...initialPosition))
  );
  const [position, setPosition] = useState(initialPositionArray);
  const [velocityList, setVelocityList] = useState(initialVelocityArray);
  const [trajectoryArray, setTrajectory] = useState(initialTrajectoryArray);
  const [accelerationsArray, setAccelerations] = useState([]);
  const positionRef = useRef(initialPositionTF);
  const velocityRef = useRef(initialVelocityTF);
  const massesRef = useRef(masses);
  const deltaTimeTensor = useMemo(() => tf.scalar(dt), [dt]);

  const quadraticCompute = useCallback(() => {
    const { newPositionTf, newVelocityTf, newAccelerationTf } = updatePositions(
      positionRef,
      velocityRef,
      deltaTimeTensor,
      position.length,
      massesRef.current
    );
    newAccelerationTf
      .array()
      .then((_accelerations) => setAccelerations(_accelerations));
    newVelocityTf.array().then((_velocities) => setVelocityList(_velocities));
    newPositionTf.array().then((_positions) => setPosition(_positions));
    tf.dispose([newAccelerationTf]);
    // Transformed to be used
    // updateTrails(
    //   newPositionList,
    //   nTimeSteps,
    //   setPosition,
    //   setTrajectory,
    //   captureTrajectoryEveryNthStep
    // );
  }, [
    positionRef,
    velocityRef,
    deltaTimeTensor,
    captureTrajectoryEveryNthStep,
  ]);

  /**
   * @typedef {object} SpaceShipForm
   * @property {string} name
   * @property {number} fuel
   * @property {number} isp
   */
  /**
   *
   * @param {SpaceShipForm} spaceShipForm
   */
  function addSpaceShip(spaceShipForm) {
    const spaceShip = { ...spaceShipForm };
    spaceShip["color"] = rColor();
    // Ship mass is 1.2 times the fuel at start;
    const mass = 1.2 * KG_MASS_CONVERSION * (1000 * spaceShipForm.fuel);
    spaceShip["m"] = mass;
    const earth = planets[3];
    const ssPosition = new Vector3()
      .fromArray(position[3])
      .multiplyScalar(1.003)
      .toArray();
    const ssVelocity = new Vector3()
      .fromArray(velocityList[3])
      .multiplyScalar(1.003)
      .toArray();
    spaceShip["x"] = ssPosition;
    spaceShip["v"] = ssVelocity;
    spaceShip["r"] = 0.0000000001;
    spaceShip["type"] = "spaceship";
    const newPlanets = [...planets];
    newPlanets[newPlanets.length] = spaceShip;
    console.log("adding", { spaceShip });

    const oldPositionTf = positionRef.current;

    positionRef.current = appendToTensor(
      oldPositionTf,
      ssPosition,
      newPlanets.length
    );
    const oldVelocityTf = velocityRef.current;
    velocityRef.current = appendToTensor(
      oldVelocityTf,
      ssVelocity,
      newPlanets.length
    );

    const newAcc = [...accelerationsArray, [0, 0, 0]];
    console.log({ accelerationsArray, newAcc });
    setAccelerations(newAcc);
    setPlanets(newPlanets);
    const newPosition = [...position, ssPosition];
    setPosition(newPosition);
    setVelocityList([...velocityList, ssVelocity]);
    setTrajectory([...trajectoryArray, ssPosition]);
    massesRef.current = [...massesRef.current, mass];
    tf.dispose([oldPositionTf, oldVelocityTf]);
  }

  return {
    quadraticCompute,
    position,
    velocityList,
    accelerationsArray,
    trajectoryArray,
    planets,
    addSpaceShip,
  };
}

export default useSolarSystem;
