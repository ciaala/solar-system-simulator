import { Vector3 } from "three";
import useParticles from "./useParticles";
import { renderHook } from "@testing-library/react-hooks";
//
describe("Test useParticles", () => {
  it("particles are created on update", async () => {
    const {
      result: { current },
      waitForNextUpdate,
    } = renderHook(() =>
      useParticles({
        count: 10,
        color: "blue",
      })
    );
    // await waitForNextUpdate();
    expect(current.particles.length).toBe(0);
    current.update(1, new Vector3(1, 2, 3), new Vector3(), true);
    expect(current.particles.length).toBe(1);
    expect(current.particles[0]).toEqual(new Vector3(1, 2, 3));

    current.update(1, new Vector3(0, 2, 0), new Vector3(), true);
    expect(current.particles.length).toBe(2);
    expect(current.particles[0]).toEqual(new Vector3(2, 4, 6));
    expect(current.particles[1]).toEqual(new Vector3(0, 2, 0));
  });
  it("keep the count uptodate", async () => {
    const {
      result: { current },
      waitForNextUpdate,
    } = renderHook(() =>
      useParticles({
        count: 10,
        color: "blue",
      })
    );

    expect(current.particles.length).toBe(0);
    for (let i = 1; i < 14; i++) {
      current.update(1, new Vector3(1, 2, 3), new Vector3(), true);
      expect(current.particles.length).toBe(Math.min(10, i));
      expect(current.particles[0]).toEqual(new Vector3(1 * i, 2 * i, 3 * i));
    }
    expect(current.particles.length).toBe(10);
  });
});
