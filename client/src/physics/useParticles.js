import { useState } from "react";
import { Vector3 } from "three";

/**
 * @typedef {object} UseParticleHook
 * @property {Array.<Vector3>} particles
 * @property {function} update
 */

/**
 *
 * @param {number} count
 * @return {UseParticleHook}
 */
function useParticles({ count }) {
  const [particles, setParticles] = useState([]);
  /**
   * @property {Vector3} velocities
   * @property {function} setVelocities
   */
  const [velocities, setVelocities] = useState([]);

  /**
   *
   * @param {number} dt
   * @param {Vector3} velocity
   * @param {Vector3} position
   * @param {boolean} isEngineOn
   *
   */
  function update(dt, velocity, position, isEngineOn) {
    let changed = false;
    if (particles.length === count) {
      particles.shift();
      changed = true;
    }
    if (particles.length > 0) {
      particles.map((particle, index) => {
        particle.addScaledVector(velocities[index], dt);
      });
      changed = true;
    }

    if (isEngineOn) {
      /** @type {Vector3} */
      const particlePosition = position.clone();
      particlePosition.addScaledVector(velocity, dt);

      velocities.push(velocity.clone());
      particles.push(particlePosition);
    }
    if (changed) {
      setParticles(particles);
    }
  }

  return { particles, update };
}

export default useParticles;
