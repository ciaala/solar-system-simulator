import equivalentForceDistance from './equivalentForceDistance';

const sut = equivalentForceDistance;

describe('testing equivalent distance between celestial bodies', () => {
  it('if both bodies have the same mass', () => {
    expect(sut(1, 1, [2, 0, 0])).toBe(1);
  });
  it('if one body is million of time greater', () => {
    const _1_1000 = sut(1, 1000, [10, 0, 0]);
    const _1_1000_000 = sut(1, 1000000, [10, 0, 0]);
    const _1_1000_000_000 = sut(1, 1000000000, [10, 0, 0]);
    expect(_1_1000).toBeGreaterThan(_1_1000_000);
    expect(_1_1000_000).toBeGreaterThan(_1_1000_000_000);
  });
  it('Earth Sun', () => {
    const au = 150 * 1000 * 1000;
    const l1 = sut(1, 333000, [au, 0, 0]);
    expect(l1).toBeGreaterThanOrEqual(au / (1 + Math.sqrt(333000)));
    expect(l1).toBeLessThanOrEqual(au / (1 + Math.sqrt(329999)));
  });
});
