function equivalentForceDistance(mass, secondMass, vectorDistance) {
  console.log({mass, secondMass});
  if (mass > secondMass) {
    console.log('Invalid equivalent force distance');
    return 0;
  }
  const squareDistance = vectorDistance.reduce(
      (acc, v) => acc + (v * v), 0);
  const dist = Math.sqrt(squareDistance);
  console.log({dist, vectorDistance});
  if (mass === secondMass) {
    return dist / 2;
  }
  if (mass !== 1) {
    secondMass = secondMass / mass;
    //mass = 1;
  }
  //const massRatio = secondMass / mass;
  //console.log(massRatio);
  const x = dist / (1 + Math.sqrt(secondMass));
  return x;
}

export default equivalentForceDistance;
