import * as tf from "@tensorflow/tfjs";
import { Vector3 } from "three";
import data from "./data.json";
import equivalentForceDistance from "./equivalentForceDistance";

/**
 * @typedef {Object} PlanetInfo
 *
 * @property {string} name the name of the body
 * @property {string} color the color
 * @property {number} m the mass
 */

function rColor() {
  function rnd256() {
    return Math.floor(Math.random() * 256);
  }

  const color = [rnd256(), rnd256(), rnd256()];
  return `rgb(${color[0]},${color[1]},${color[2]})`;
}

const KG_MASS_CONVERSION = 0.00000000001;

const earth = data.planets[3];
// , [1, 0], [0, 1], [0, -1]
let moons = [];
if (moons && false) {
  moons = [[0, -1]].map((index) => {
    const x = [
      earth.x[0] + 0.00268 * index[0],
      earth.x[1] + 0.00268 * index[1],
      earth.x[2],
    ];
    const v = [...earth.v];
    console.log(v);

    const k = 30;
    v[2] = v[2] / k;
    v[1] = v[1] / k;
    v[0] = v[0] / k;
    // console.log(v);
    //-= 0.0001;
    return {
      x,
      v,
      m: earth.m / 0.0123,
      color: rColor(),
      name: "Moon" + index,
      r: earth.r * 0.273,
    };
  });
}
let bodies = [];
if (bodies && false) {
  bodies = [
    {
      type: "spaceship",
      name: "USS - Pride of the Dolphins",
      color: "blue",
      x: [...earth.x],
      m: 1000 * KG_MASS_CONVERSION,
      v: [...earth.v],
    },

    {
      type: "spaceship",
      name: "USS - FishMongers",
      color: "green",
      x: [...earth.x],
      m: 1000 * KG_MASS_CONVERSION,
      v: [...earth.v],
    },
    {
      type: "spaceship",
      name: "USS - Miami Dolphins",
      color: "red",
      x: [...earth.x],
      m: 1000 * KG_MASS_CONVERSION,
      v: [...earth.v],
    },
  ];

  const potd = new Vector3().fromArray(bodies[0].x).multiplyScalar(1.01);
  potd.toArray(bodies[0].x);

  console.log({ bodies, e: earth });
}

const planets = data.planets.concat(moons).concat(bodies);
console.info({ planets });
const sun = data.planets[0];
const {
  x: initialPositionArray,
  v: initialVelocityArray,
  m: masses,
  eqRadius,
} = planets.reduce(
  (acc, planet) => {
    return {
      x: acc.x.concat([planet.x]),
      v: acc.v.concat([planet.v]),
      m: acc.m.concat([planet.m]),
      eqRadius: acc.eqRadius.concat([
        equivalentForceDistance(planet.m, sun.m, planet.x),
      ]),
    };
  },
  { x: [], v: [], m: [], eqRadius: [] }
);

// Debug here
console.log({ eqRadius, masses });

eqRadius.forEach((v, i) => (planets[i].eqRadius = v));

function init() {
  const numberOfPlanets = initialPositionArray.length;
  return [
    tf.tensor2d(initialPositionArray, [numberOfPlanets, 3]),
    tf.tensor2d(initialVelocityArray, [numberOfPlanets, 3]),
  ];
}

const [initialPositionTF, initialVelocityTF] = init();

const G = tf.scalar(data.G);

console.log(tf.getBackend());

/**
 *
 * @param {Tensor} position
 * @param {number} numberOfBodies
 * @param {number[]} masses
 * @returns {Tensor}
 */
function calculateNextAccelerations(position, numberOfBodies, masses) {
  const unstackedPosition = tf.unstack(position);

  const forces = Array(numberOfBodies).fill(tf.tensor1d([0, 0, 0]));

  for (let i = 0; i < numberOfBodies; i++) {
    const currentPlanetPosition = unstackedPosition[i];
    for (let j = i + 1; j < numberOfBodies; j++) {
      const otherPlanetPosition = unstackedPosition[j];
      const distanceVector = tf.sub(otherPlanetPosition, currentPlanetPosition);
      const distance = tf.norm(distanceVector);

      const force = G.mul(masses[i])
        .mul(masses[j])
        .div(tf.pow(distance, 3))
        .mul(distanceVector);

      forces[i] = forces[i].add(force);
      forces[j] = forces[j].sub(force);
    }
  }

  const accelerationsTF = forces.map((force, i) => force.div(masses[i]));
  return tf.stack(accelerationsTF);
}

export {
  calculateNextAccelerations,
  initialPositionTF,
  initialVelocityTF,
  initialVelocityArray,
  initialPositionArray,
  planets,
  masses,
  rColor,
  KG_MASS_CONVERSION,
};
