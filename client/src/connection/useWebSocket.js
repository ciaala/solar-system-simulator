import {
  useState,
  createContext,
  useContext,
  useEffect,
} from 'react';

/**
 *
 * @param {string} type
 * @param {Event} message
 */
function printEvent(type, message) {
  // currentTarget.url

  //console.log(
   //   `received <${message.type}> [${type}] ${message.data ? message.data.length: null}`);
}

/**
 *
 * @type {React.Context<{webSocket: WebSocket, isConnected: boolean, userId}>}
 */
//const ServerWebSocketContext = createContext(makeInitialState('ws://localhost:3001', 'default_user_id'));
const ServerWebSocketContext = createContext(null);
function makeInitialState(serverUrl, userId) {
  // console.log('initialState',new Error().stack);

  return {
    isConnected: false,
    webSocket: new WebSocket(serverUrl),
    userId: userId,
  };
}

/**
 *
 * @param {string} serverUrl
 * @param {string} userId
 * @param children
 * @return {JSX.Element}
 * @constructor
 */
function ServerWebSocketContextProvider({serverUrl, userId, children}) {
  // console.log('ServerWebSocketContextProvider');
  /**
   *
   * @type {{webSocket: WebSocket, isConnected: boolean, userId}}
   */
  const initialState = makeInitialState(serverUrl, userId);

  const [state, dispatch] = useState(initialState);
  return <ServerWebSocketContext.Provider value={[state, dispatch]}>
    {children}
  </ServerWebSocketContext.Provider>;
}

/**
 *
 * @param {function(message: string)} receive
 * @return {{isConnected: boolean,
 * close: () => void,
 * send: function(message:object)}}
 */
function useServerWebSocketContext(receive) {
  const context = useContext(ServerWebSocketContext);
  // console.log({serverWebSocketContext: context, stack: new Error().stack.split('\n')});
  const [state, dispatch] = context;
  useEffect(() => {
    if ( !state.receive) {
      dispatch(currentState => ({...currentState, receive}));
    }
  }, [receive]);
  state.webSocket.onopen = (openEvent) => {
    printEvent('open', openEvent);
    const newState = {...state};
    newState.isConnected = true;
    dispatch(newState);
  };

  state.webSocket.onmessage = (messageEvent) => {
    printEvent('message', messageEvent);
    const message = JSON.parse(messageEvent.data);
    state.receive(message);
    // const newMessages = [...messages];
    // newMessages.push(message);
    // setMessages(newMessages);
  };

  state.webSocket.onclose = (closeEvent) => {
    printEvent('close', closeEvent);
    dispatch(currentState => ({...currentState, isConnected: false}));
  };

  state.webSocket.onerror = (errorEvent) => {
    printEvent('error', errorEvent);
    state.isConnected.current = false;
  };

  /**
   *
   * @param {object} data
   */
  function send(data) {

    if (state.isConnected) {
      if (typeof data === 'object') {
        state.webSocket.send(JSON.stringify(data));
      } else {
        console.error('Data type not handled ' + (typeof data));
      }
    } else {
      throw new Error('Ws not ready !');
    }
  }

  return {
    'isConnected': state.isConnected,
    close,
    send,
  };

}

export {
  ServerWebSocketContext,
  ServerWebSocketContextProvider,
  useServerWebSocketContext,
};
