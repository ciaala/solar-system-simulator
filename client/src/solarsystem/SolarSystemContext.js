import {createContext, useContext, useState} from 'react';

/**
 *
 * @type {React.Context<unknown>}
 */
// const SolarSystemContext = createContext({'default': 'invalid context'});
const SolarSystemContext = createContext(null);

/**
 *
 * @param props
 * @return {JSX.Element}
 * @constructor
 */
function SolarSystemContextProvider(props) {
  console.log('SolarSystemContextProvider');
  const initialState = {
    currentSelection: {
      type: undefined,
      reference: undefined,
    },
  };
  const [state, dispatch] = useState(initialState);
  return <SolarSystemContext.Provider value={[state,dispatch]}>
    {props.children}
  </SolarSystemContext.Provider>
}

/**
 *
 * @return {{updateSelection: updateSelection, currentSelection: *}}
 */
function useSolarSystemContext() {
  const context = useContext(SolarSystemContext);
  console.log({SolarSystemContext: context});
  const [state, dispatch] = [context,context];


  function updateSelection(type, reference) {
    dispatch(newState =>
        ({
          ...newState,
          currentSelection: {type, reference},
        }));
  }

  return {updateSelection, currentSelection: state.currentSelection};
}

export {SolarSystemContext, useSolarSystemContext, SolarSystemContextProvider};
