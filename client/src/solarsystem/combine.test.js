import { Vector3 } from "three";
import combine from "./combine";

describe("test combine", () => {
  it("", () => {
    const actual = combine([{planet: 'x'}], [[1, 2, 3]], [10, 20, 30], [[100, 200, 300]]);

    expect(actual).toEqual([
      new Vector3(10, 20, 30),
      { planet: "x" },
      new Vector3(10, 20, 30),
      new Vector3(100, 200, 300),
      new Vector3(1, 2, 3),
    ]);
  });
});
