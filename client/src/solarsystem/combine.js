import { Vector3 } from "three";

/**
 *
 * @param {CelestialBody} celestialBody
 * @param {number[]} position
 * @param {number[]} acceleration
 * @param {number[]} velocity
 * @return {number[],CelestialBody,Vector3,Vector3,Vector3,Vector3]}
 */

function combine(celestialBody, position, acceleration, velocity) {
  if (!position || !velocity || !acceleration || !celestialBody ) {
    console.log("Fuck !");
  }
  const from = new Vector3().fromArray(position);
  const av = new Vector3().fromArray(acceleration);
  const velocityVector = new Vector3()
    .fromArray(velocity)
    .multiplyScalar(5);

  const accelerationVector3 = new Vector3(0, 0, 0).add(from).add(av);

  const velocityVector3 = new Vector3(0, 0, 0).add(from).add(velocityVector);
  return [acceleration, celestialBody, accelerationVector3, velocityVector3, from];
}

export default combine;
