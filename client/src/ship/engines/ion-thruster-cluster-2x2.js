import PropTypes from "prop-types";
import { Suspense } from "react";
import { Vector3 } from "three";
import GLTF from "../../components/GLTF";
import { useGLTF } from "@react-three/drei";

useGLTF.preload("/resources/ion-engine/ion-thruster-cluster-2x2.glb");

function IonThrusterCluster2x2(props) {
  return (
    <Suspense fallback={null}>
      <GLTF
        model={"/resources/ion-engine/ion-thruster-cluster-2x2.glb"}
        position={props.position}
        rotation={props.rotation}
      />
    </Suspense>
  );
}

IonThrusterCluster2x2.propTypes = {
  rotation: PropTypes.instanceOf(Vector3).isRequired,
  position: PropTypes.instanceOf(Vector3).isRequired,
};

export default IonThrusterCluster2x2;
