import { Text } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import PropTypes from "prop-types";
import { Suspense } from "react";
import { Vector3, Euler } from "three";
import { getTextPosition } from "../components/PlanetDetail";
import useParticles from "../physics/useParticles";
import GLTF from "../components/GLTF";
import JustBox from '../components/JustBox';
import {useSolarSystemContext} from '../solarsystem/SolarSystemContext';

function IonParticle(props) {
  return (
    <group position={props.particle.toArray()}>
      {/* it should not use pointLight but directional light  */}
      <pointLight color={props.color} intensity={0.01} scale={0.1} />
      <mesh>
        <sphereGeometry args={[0.01, 2, 2]} />
        <meshBasicMaterial color={props.color} />
      </mesh>
    </group>
  );
}
/**
 *
 * @param {Vector3} props.position
 * @param {Vector3} props.acceleration
 * @param {Vector3} props.orientation
 * @param {number} props.planetGravityField
 * @param {boolean} props.isEngineOn
 * @param {number} props.dt
 * @param {object} props.spaceShip
 * @returns {JSX.Element}
 * @constructor
 */
function SpaceShip(props) {
  // const {updateSelection} = useSolarSystemContext();
  const enginePosition = props.position.clone();
  enginePosition.add(new Vector3(-1, -1.5, 0.5));
  const particlePosition = enginePosition.clone().add(new Vector3(-1, -2, -1));
  //const engineRotation = new Vector3(0, 0, Math.PI / 2);
  const velocity = props.orientation;

  const particleHook = useParticles({ count: 100 });
  useFrame(() => {
    if (particleHook.particles.length > 0 || props.isEngineOn) {
      particleHook.update(
        props.dt,
        velocity,
        particlePosition,
        props.isEngineOn
      );
    }
  });
  const engineRotation = new Vector3(0, 0, Math.PI);
  const r = new Euler().setFromVector3(velocity);
  return (
    <Suspense fallback={<JustBox color="green" position={props.position.toArray([])} size={[1, 1, 1]}/>}>

      <group
        scale={props.scale}
        position={props.position}
        // rotation={r}
        onPointerOver={() => updateSelection('spaceship', props.spaceShip)}
      >
        <JustBox color="green" position={[0,0,0]} size={[10, 10, 10]}/>
        <GLTF
          model={"/resources/rings-space-ship/rings-space-ship.glb"}
          position={new Vector3()}
          rotation={new Vector3()}
        />
        <GLTF
          model={"/resources/ion-engine/ion-thruster-cluster-2x2-particle.glb"}
          position={enginePosition}
          rotation={engineRotation}
        />
        <Text
            scale={[20, 20, 20]}
            color={props.spaceShip.color}
            //rotation={textRotation}
            //position={getTextPosition(props.position)}
            position={new Vector3(0,3,0)}
        >
          {props.spaceShip.name}
        </Text>
      </group>
      {false ? particleHook.particles.map((particle, index) => (
        <IonParticle
          key={"particle-" + index}
          particle={particle}
          color={props.spaceShip.color}
          rotation={engineRotation}
        />
      )) : null}

      {/*<PlanetDetail*/}
      {/*  position={props.position}*/}
      {/*  acceleration={props.acceleration}*/}
      {/*  color={props.spaceShip.color}*/}
      {/*  velocity={props.orientation}*/}
      {/*  planet={props.spaceShip}*/}
      {/*  planetGravityField={props.planetGravityField}*/}
      {/*/>*/}
    </Suspense>
  );
}

SpaceShip.defaultProps = {
  scale: 1,
};
SpaceShip.propTypes = {
  acceleration: PropTypes.instanceOf(Vector3).isRequired,
  orientation: PropTypes.instanceOf(Vector3).isRequired,
  position: PropTypes.instanceOf(Vector3).isRequired,
  planetGravityField: PropTypes.number.isRequired,
  scale: PropTypes.number,
  isEngineOn: PropTypes.bool.isRequired,
  dt: PropTypes.number.isRequired,
  spaceShip: PropTypes.shape({
    color: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
};

export default SpaceShip;
