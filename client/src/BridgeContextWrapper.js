import {useContextBridge} from '@react-three/drei';
import {ServerWebSocketContext} from './connection/useWebSocket';
import {SolarSystemContext} from './solarsystem/SolarSystemContext';

/**
 *
 * @param children
 * @return {JSX.Element}
 * @constructor
 */
export function BridgeContextWrapper({children}) {
  const BridgedContext = useContextBridge(ServerWebSocketContext, SolarSystemContext);

  return <BridgedContext>{children}</BridgedContext>;
}
