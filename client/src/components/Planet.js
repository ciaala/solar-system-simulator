import PropTypes from 'prop-types';

function Planet(props) {
  const smooth = props.radius > 0.2 ? 32 : 16;
  return (
      <mesh position={props.position} onPointerOver={props.onPointerOver}>
        <sphereBufferGeometry
            args={[props.radius, smooth, smooth]}
            attach="geometry"
        />
        <meshStandardMaterial color={props.color} attach="material"/>
      </mesh>
  );
}

Planet.propTypes = {
  position: PropTypes.arrayOf(PropTypes.number),
  radius: PropTypes.number,
  color: PropTypes.string,
};
export default Planet;
