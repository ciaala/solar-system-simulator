import {BufferGeometry, Vector3} from 'three';
import * as PropTypes from 'prop-types';

function Trajectory(props) {
  const bufferGeometry = new BufferGeometry().setFromPoints(props.points);
  return (
      <line>
        <primitive attach="geometry" object={bufferGeometry}/>
        <lineBasicMaterial color={props.color} linewidth={20}/>
      </line>
  );
}

const Vector3Shape = PropTypes.shape({
  x: PropTypes.number,
  y: PropTypes.number,
  z: PropTypes.number,
});
Trajectory.propTypes = {
  color: PropTypes.string.isRequired,
  points: PropTypes.arrayOf(Vector3Shape).isRequired,
};

export default Trajectory;
