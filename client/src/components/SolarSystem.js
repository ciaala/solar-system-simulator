import * as PropTypes from 'prop-types';
import {Suspense} from 'react';
import {useEffect, useRef, useState} from 'react';
import {Vector3} from 'three';
import {useFrame} from '@react-three/fiber';
import Planet from './Planet';
import PlanetDetail from './PlanetDetail';
import SpaceShip from '../ship/SpaceShip';
import Trajectory from './Trajectory';
import combine from '../solarsystem/combine';
import {useServerWebSocketContext} from '../connection/useWebSocket';
import JustBox from './JustBox';
import GLTF from './GLTF';
import {useSolarSystemContext} from '../solarsystem/SolarSystemContext';

const DEFAULT_DELTA_TIME = 0.1;

const errors = {};

function makePlanetElements(position, planets, celestialBodySelection) {
  return position
  // .filter((value, index) => selections.has(index))
  .map((planetPosition, i) => [planetPosition, planets[i]]).filter(([planetPosition, planet]) =>
      isVisible(celestialBodySelection, planet.type) &&
      (planet.type === 'planet' || planet.type === 'star')).map(([planetPosition, planet], i) => {
    return (
        <Planet
            key={`planet-${i}`}
            color={planet.color}
            position={planetPosition}
            radius={i === 0 ? planet.r * 5 : planet.r * 40}
        />
    );
  });
}

/**
 *
 * @param {object<string,boolean>} celestialBodySelection
 * @param {string} type
 * @return {*}
 */
function isVisible(celestialBodySelection, type) {
  return celestialBodySelection && celestialBodySelection[type];
}

/**
 *
 * @param {number[][]} accelerations
 * @param {object[]}planets
 * @param {number[][]} positions
 * @param {number[][]} velocities
 * @param {number} planetGravityField
 * @param {number} dt
 * @return {JSX.Element[]|null}
 */
function makePlanetDetails(accelerations, planets, positions, velocities, planetGravityField, dt,
                           celestialBodySelection) {
  return accelerations.length !== 0
      ? accelerations
      //     .filter((value, index) => selections.has(index))

      .map((acceleration, index) => combine(planets[index], positions[index], acceleration, velocities[index])).
      filter(([_, planet]) => isVisible(celestialBodySelection, planet.type)).
      map(([a, celestialBody, acceleration, velocity, from], index) => {
        if (celestialBody.type === 'spaceship') {
          return (true ? <SpaceShip
                      key={`spaceship-${index}`}
                      position={from}
                      orientation={velocity}
                      acceleration={acceleration}
                      spaceShip={celestialBody}
                      planetGravityField={planetGravityField}
                      dt={dt}
                      isEngineOn={false}
                      scale={0.1}
                  /> :
                  <JustBox color="green" position={from.toArray([])} size={[1, 1, 1]}/>
          );
        } else {
          return (
              <PlanetDetail
                  key={`detail-${index}`}
                  position={from}
                  acceleration={acceleration}
                  color={celestialBody.color}
                  velocity={velocity}
                  planet={celestialBody}
                  planetGravityField={planetGravityField}
              />
          );
        }
      })
      : null;
}

/**
 *
 * @param {React.MutableRefObject<number>} timeRef
 * @param {number} planetGravityField
 * @param {Object<string,boolean>}celestialBodySelection
 * @return {JSX.Element}
 * @constructor
 */
function SolarSystem({timeRef, planetGravityField, celestialBodySelection}) {

  // const [planetsRef  ] = useRef([]);
  // const [positionsRef ] = useRef([]);
  // const [velocitiesRef ] = useRef([]);
  // const [accelerationsRef ] = useRef([]);

  // const dataRef = useRef({});
  const planetoidsRef = useRef({});
  const [planets, setPlanets] = useState([]);
  const [positions, setPositions] = useState([]);
  const [velocities, setVelocities] = useState([]);
  const [accelerations, setAccelerations] = useState([]);

  const isMounted = useRef(true);
  useEffect(() =>
      () => {
        isMounted.current = false;
      }, []);

  function receiveMessage(message) {
    if (isMounted.current && message && message.type === 'update' && message.data) {
      const data = {... message.data, counter: message.counter };
      planetoidsRef.current.planetElements = makePlanetElements(
          data.positions,
          data.planets,
          data.celestialBodySelection);
      planetoidsRef.current.planetDetails = makePlanetDetails(data.accelerations,
          data.planets,
          data.positions,
          data.velocities,
          data.planetGravityField,
          0.1,
          celestialBodySelection);
      timeRef.current = data.counter;

    }
  }

  useFrame((state, delta) => {
    if ( planetoidsRef.current ) {
      setPlanetElements(planetoidsRef.current.planetElements);
      // TODO ffi 0.1
      setPlanetDetails(planetoidsRef.current.planetDetails);
    }
  });

  // const selectionContext = useSolarSystemContext();
  const context = useServerWebSocketContext(receiveMessage);
  // console.log({useWebServerContext: context});
  const {isConnected} = context;

  // const selections = new Set([0,1,2,3,4,11]);
  const [planetElements, setPlanetElements] = useState([]);
  const [planetDetails, setPlanetDetails] = useState([]);

  // useEffect(() => {
  //
  // }, [positions, planets, accelerations, velocities, planetGravityField]);

  useEffect(() => {
    console.log({isConnected});
  }, [isConnected]);

  const trajectories = undefined;
  // trajectoryArray
  //   //  .filter((value, index) => selections.has(index))
  //   .map((points, i) => [points, planets[i]])
  //   .map(([points, planet], i) => (
  //     <Trajectory
  //       key={`trajectory-${i}`}
  //       points={points}
  //       color={planet.color}
  //     />
  //   ));

  if (
      ((planetDetails && planetElements.length !== planetDetails.length) ||
          (trajectories && planetElements.length !== trajectories.length)) &&
      !errors['not the same number of elements']
  ) {
    errors['not the same number of elements'] = true;
    console.error('not the same number of elements', {
      planetDetails: planetDetails.length,
      trajectories: trajectories.length,
      planetElements: planetElements.length,
    });
  }

  return (
      <>
        <group>
          {planetElements}
          {trajectories ? trajectories : null}
          {planetDetails ? planetDetails : null}
        </group>
        {/*<Billboard>*/}
        {/*  <Text color="white">{totalTimeRef.current.toFixed(1)}</Text>*/}
        {/*</Billboard>*/}
      </>
  );
}

SolarSystem.defaultProps = {
  dt: DEFAULT_DELTA_TIME,
  planetGravityField: 1,
};
export default SolarSystem;

