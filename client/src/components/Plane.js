import PropTypes from "prop-types";
import { useFrame, useThree } from "@react-three/fiber";
import { useMemo, useState } from "react";
import { Stars } from "@react-three/drei";

/**
 *
 * @param {number[]} firstSeq
 * @param {number[]} secondSeq
 */
function isSameValueElements(firstSeq, secondSeq) {
  return firstSeq.every((value, index) => secondSeq[index] === value);
}

/**
 *
 * @param {string} color
 * @returns {JSX.Element}
 * @constructor
 */
function Plane({ color }) {
  const { camera } = useThree();
  const [cameraPosition, setCameraPosition] = useState([]);

  const [size, fragment] = useMemo(() => {
    const size = camera.position.length() * 2;
    const fragment = 10 + Math.floor(Math.log2(size)) * 4;
    return [size, fragment];
  }, [cameraPosition]);

  useFrame(() => {
    if (!isSameValueElements(camera.matrix.elements, cameraPosition)) {
      const cameraDistance = camera.position.length() * 2;
      if (Math.abs(Math.floor(cameraDistance) - Math.floor(size)) > 2) {
        setCameraPosition(Array.from(camera.matrix.elements));
      }
    }
  });

  return (
    <>
      {/*<Stars*/}
      {/*  radius={2 * size}*/}
      {/*  depth={size}*/}
      {/*  count={1000 * Math.log2(size)}*/}
      {/*  factor={4}*/}
      {/*  saturation={0}*/}
      {/*  fade*/}
      {/*/>*/}
      <mesh>
        <planeBufferGeometry
          attach="geometry"
          args={[size, size, fragment, fragment]}
        />
        <meshBasicMaterial
          color={color}
          wireframe={true}
          opacity={0.1}
          transparent={true}
        />
      </mesh>
    </>
  );
}

Plane.propTypes = {
  color: PropTypes.string,
};
export default Plane;
