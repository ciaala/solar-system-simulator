/**
 *
 * @param {string} color
 * @param {number[]} position
 * @param {number[]} size
 * @returns {JSX.Element}
 * @constructor
 */
function JustBox({ color, position, size }) {
  return (
    <mesh position={position}>
      <boxBufferGeometry attach={"geometry"} args={size} />
      <meshBasicMaterial color={color} wireframe={true} />
    </mesh>
  );
}

export default JustBox;
