import {Vector3} from 'three';
let tailTime = 0;
let tailCounter = 0;
const TRAIL_SEGMENTS_LENGTH = 128 * Math.PI;


function updateTrails(
    newPositionList,
    timeSteps,
    setPosition,
    setTrajectory,
    captureTrajectoryEveryNthStep,
) {
  timeSteps.current++;
  newPositionList.array().then((newPosition) => {
    setPosition(newPosition);
    if (timeSteps.current % captureTrajectoryEveryNthStep === 0) {
      tailCounter++;
      setTrajectory((trajectory) => {
        const start = window.performance.now();
        const result = trajectory.map((points, index) => {
          const trailPoint = new Vector3(...newPosition[index]);
          const adjustedTrailLength =
              TRAIL_SEGMENTS_LENGTH * (index * (index / 2));
          if (points.length >= adjustedTrailLength) {
            points = points.slice(-adjustedTrailLength + 1);
          }
          points.push(trailPoint);
          return points;
        });
        const end = window.performance.now();
        const current = end - start;
        tailTime += current;

        return result;
      });
    }
  });
  if (tailCounter % 100 === 0) {
    console.log('Trailing: ', {
      batch: tailCounter / 100,
      bench: tailTime / 100,
      // last: current,
    });
    tailTime = 0;
  }
}

export default updateTrails;
