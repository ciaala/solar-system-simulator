import { useThree } from "@react-three/fiber";
import { useRef } from "react";
import { Vector3 } from "three";
import { OrbitControls, GizmoHelper, GizmoViewport } from "@react-three/drei";

function Renderer(props) {
  const { camera } = useThree();
  const controlsRef = useRef();
  camera.up = new Vector3(0, 0, 1);
  camera.lookAt(0, 0, 0);

  return (
    <>
      <OrbitControls camera={camera} ref={controlsRef} />
      <ambientLight />
      <pointLight />
      {props.children}
      <GizmoHelper
        alignment={"top-left"}
        margin={[80, 80]}
        onTarget={() => controlsRef?.current?.target}
        onUpdate={() => controlsRef.current?.update()}
      >
        <GizmoViewport
          axisColors={["red", "green", "blue"]}
          labelColor={"black"}
        />
      </GizmoHelper>
    </>
  );
}

export default Renderer;
