import PropTypes from "prop-types";
import { useGLTF } from "@react-three/drei";
import { useRef } from "react";
import { Vector3 } from "three";

useGLTF.preload([
  "/resources/rings-space-ship/rings-space-ship.glb",
  "/resources/ion-engine/ion-thruster-cluster-2x2.glb",
  "/resources/ion-engine/ion-thruster-cluster-2x2-particle.glb",
]);

function GLTF(props) {
  const group = useRef();
  const { nodes } = useGLTF(props.model);
  return (
    <group ref={group} position={props.position} dispose={null}>
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cylinder.geometry}
        material={nodes.Cylinder.material}
        rotation={props.rotation.toArray()}
      />
    </group>
  );
}

GLTF.propTypes = {
  model: PropTypes.string.isRequired,
  // orientation: PropTypes.object.isRequired,
  position: PropTypes.instanceOf(Vector3).isRequired,
  rotation: PropTypes.instanceOf(Vector3).isRequired,
};
export default GLTF;
