import { Text } from "@react-three/drei";
import PropTypes from "prop-types";
import { Vector3 } from "three";
import Line from "./Line";

const origin = new Vector3();

/**
 *
 * @param {Vector3} position
 * @return {Vector3}
 */
export function getTextPosition(position) {
  const nPosition = new Vector3().copy(position).normalize();
  return new Vector3().copy(nPosition).multiplyScalar(0.05);
}

/**
 *
 * @param {Vector3} position
 * @param {Vector3} acceleration
 * @param {Vector3} velocity
 * @param {PlanetInfo} planet
 * @param {number} planetGravityField
 * @returns {JSX.Element}
 * @constructor
 */
function PlanetDetail({
  position,
  acceleration,
  velocity,
  planet,
  planetGravityField,
}) {
  let force1over10 = planet.eqRadius
    ? planet.eqRadius * Math.sqrt(planetGravityField)
    : 0;
  force1over10 = 0;

  const textPosition = getTextPosition(position);

  // new Plane(position);
  //const angle = position.angleTo(velocity);
  // const nVelocity = new Vector3().copy(velocity).normalize();
  // const q = new Quaternion().setFromUnitVectors(nPosition, nVelocity);
  // const textRotation = new Euler().setFromQuaternion(q);

  return (
    <>
      <Line from={origin} to={position} color="purple" />
      <Line from={position} to={acceleration} color={planet.color} />
      <Line
        from={position}
        to={new Vector3(position.x, position.y, 0)}
        color="gray"
      />
      <Line from={position} to={velocity} color="#FF0080" />
      <group position={position}>
        {/*<Line from={[0, 0, 0]} to={[1, 0, 0]} color="#FF0080" />*/}

        {force1over10 ? (
          <mesh>
            <circleBufferGeometry args={[force1over10, 16]} />
            <meshBasicMaterial color="#604040" wireframe={true} />
          </mesh>
        ) : null}
        <Text
          scale={[0.3, 0.3, 0.3]}
          color={planet.color}
          //rotation={textRotation}
          position={textPosition}
        >
          {planet.name}
        </Text>

        {/*<Text*/}
        {/*  scale={[0.3, 0.3, 0.3]}*/}
        {/*  color={"green"}*/}
        {/*  rotation={textRotation}*/}
        {/*  position={textPosition}*/}
        {/*>*/}
        {/*  {planet.name} !*/}
        {/*</Text>*/}
      </group>
    </>
  );
}
PlanetDetail.propTypes = {
  acceleration: PropTypes.object,
  color: PropTypes.string,
  planet: PropTypes.object,
  position: PropTypes.object,
  velocity: PropTypes.object,
};

export default PlanetDetail;
