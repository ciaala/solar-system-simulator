import * as tf from "@tensorflow/tfjs";
import { calculateNextAccelerations } from "../physics/physics";

/**
 * @param {Object} positionRef
 * @param {Tensor} positionRef.current
 * @param {Object} velocityRef
 * @param {Tensor} velocityRef.current
 * @param {Tensor} deltaTimeTensor
 * @param {number} numberOfBodies
 * @param {number[]} masses
 * @returns {{newPositionTf: Tensor, newVelocityTf: Tensor, newAccelerationTf : Tensor}} new position list Tensor
 */
function updatePositions(
  positionRef,
  velocityRef,
  deltaTimeTensor,
  numberOfBodies,
  masses
) {
  let currentPosition = positionRef.current;
  let currentVelocity = velocityRef.current;
  /**
   * @type [Tensor,Tensor,Tensor]
   */
  const [newPositionTf, newVelocityTf, newAccelerationTf] = tf.tidy(() => {
    const newAccelerationTf = calculateNextAccelerations(
      currentPosition,
      numberOfBodies,
      masses
    );

    const newVelocityTf = currentVelocity.add(
      tf.mul(newAccelerationTf, deltaTimeTensor)
    );

    const newPositionTf = currentPosition
      .add(tf.mul(currentVelocity, deltaTimeTensor))
      // use newVelocityTf
      .add(tf.mul(newAccelerationTf, deltaTimeTensor).mul(deltaTimeTensor));
    return [newPositionTf, newVelocityTf, newAccelerationTf];
  });
  tf.dispose([velocityRef.current, positionRef.current]);

  positionRef.current = newPositionTf;
  velocityRef.current = newVelocityTf;

  return { newPositionTf, newVelocityTf, newAccelerationTf };
}

export default updatePositions;
