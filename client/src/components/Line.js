import * as PropTypes from 'prop-types';
import {BufferGeometry} from 'three';

function Line(props) {
  const points = [props.from, props.to];
  const bufferGeometry = new BufferGeometry().setFromPoints(points);
  return (
      <line>
        <primitive attach="geometry" object={bufferGeometry}/>
        <lineBasicMaterial color={props.color} linewidth={20} opacity={0.5} transparent={true}/>
      </line>
  );
}

const vectorPropType = {
  x: PropTypes.number,
  y: PropTypes.number,
  z: PropTypes.number,
};
Line.propTypes = {
  from: PropTypes.shape(vectorPropType).isRequired,
  to: PropTypes.shape(vectorPropType).isRequired,
  color: PropTypes.string.isRequired,
};
export default Line;
