import { Vector3 } from "three";
import Line from "./Line";

const origin = new Vector3(0, 0, 0);
const axis_x = new Vector3(1000, 0, 0);
const axis_y = new Vector3(0, 1000, 0);
const axis_z = new Vector3(0, 0, 1000);

function Axis() {
  return (
    <>
      <Line color="red" from={origin} to={axis_x} />
      <Line color="green" from={origin} to={axis_y} />
      <Line color="#5050FF" from={origin} to={axis_z} />
    </>
  );
}

export default Axis;
