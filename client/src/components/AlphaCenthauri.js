import Planet from "./Planet";

function AlphaCentauri({ au, sunRadius }) {
  const ly = au * 63241;
  return (
    <group position={ly * 4.2465}>
      <Planet color="gold" position={ly * 0.13} />
      <Planet color="orange" position={ly * 0.13} radius />
      <Planet color="brown" radius={sunRadius * 10} />
    </group>
  );
}

export default AlphaCentauri;
