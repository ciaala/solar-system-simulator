import {buttonGroup, folder, useControls} from 'leva';
import * as PropTypes from "prop-types";
import {useSolarSystemContext} from '../solarsystem/SolarSystemContext';

// import {Vector3} from 'three';
/**
 *
 * @param props
 * @return {null}
 * @constructor
 */
function UI(props) {
  // const {position, velocity} = useControls(
  //     {
  //       position: new Vector3(),
  //       velocity: new Vector3()
  //     });

  // return <div><div>{position.x}</div><div>{velocity.x}</div></div>;
  // const {time, zoom} =

  // const disabled = {
  //   zoom: {
  //     value: props.zoom,
  //     onChange: value => {
  //       props.setZoom(value);
  //     },
  //     min: 0.2,
  //     max: 10,
  //     step: 0.2,
  //   },
  // };
  // const {currentSelection} = useSolarSystemContext();
  const celestialBodySelector = Object.entries(props.celestialBodySelection).reduce( (acc, [key,value]) => {
    acc[key] = {
        value,
        onChange: (current) => {
          props.setCelestialBodySelection(newState => { newState[key] = current; return newState;});
        }
    }
    return acc;
  }, {});
  // console.log(celestialBodySelector);
  const [values, set] = useControls(() => ({
    time: {
      value: props.time,
      onChange: (value) => {
        props.setTime(value);
      },
      min: 0.0,
      max: 25,
      step: 0.05,
    },
    timeButtons: buttonGroup({
      label: "",
      opts: {
        "x 0": () => set({ ...values, time: 0 }),
        "x .05": () => set({ ...values, time: 0.05 }),
        "x .1": () => set({ ...values, time: 0.1 }),
        "x .5": () => set({ ...values, time: 0.5 }),
        "x 1": () => set({ ...values, time: 1 }),
        "x 2": () => set({ ...values, time: 2 }),
        "x 5": () => set({ ...values, time: 5 }),
        "x 25": () => set({ ...values, time: 25 }),
      },
    }),

    planetGravityField: {
      value: props.planetGravityField,
      onChange: (value) => {
        props.setPlanetGravityField(value);
      },
      min: 1,
      max: 100,
      step: 1,
    },
    celestialBody: folder({
      ...celestialBodySelector
    }),
    // selection: folder ( {
    //   currentSelection: currentSelection ? `${currentSelection.type}` : 'no spaceship selected'
    // })
  }));

  return null;
}

UI.propTypes = {
  planetGravityField: PropTypes.number,
  setPlanetGravityField: PropTypes.func,
  time: PropTypes.number,
  setTime: PropTypes.func,
  celestialBodySelection: PropTypes.object.isRequired,
  setCelestialBodySelection: PropTypes.func,
};
export default UI;
