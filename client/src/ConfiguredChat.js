import Chat from './routes/Chat';
import {ServerWebSocketContextProvider} from './connection/useWebSocket';

import {
  uniqueNamesGenerator,
  adjectives,
  colors,
  animals,
} from 'unique-names-generator';

const conf = {dictionaries: [adjectives, colors, animals]};

function unique() {
  return uniqueNamesGenerator(conf);
}

export function ConfiguredChat() {

  const userId = unique();
  return <ServerWebSocketContextProvider serverUrl={"ws://localhost:3001"} userId={userId}>
    <Chat userId={userId}
          // messages={messages} setMessages={setMessages}
    />
  </ServerWebSocketContextProvider>;
}
