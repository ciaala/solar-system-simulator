import {adjectives, animals, colors} from 'unique-names-generator';

const conf = {dictionaries: [adjectives, colors, animals]};

export function unique() {
  return uniqueNamesGenerator(conf);
}
