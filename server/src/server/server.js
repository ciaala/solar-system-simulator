import * as http from 'http';
import express from 'express';
import expressWs from 'express-ws';
import cors from 'cors';
import {makeDefaultSolar} from '../engine/solar.js';
import AddSpaceShip from '../engine/commands/AddSpaceShip.js';

const solar = makeDefaultSolar();
/**
 *
 * @type {Set<WebSocket>}
 */
const webSockets = new Set();

const mapWsToConnections = new Map();

class ClientError extends Error {
  /**
   * @type {string}
   */
  message;

  /**
   *
   * @param {string} message
   */
  constructor(message) {
    super(`[CLIENT ERROR] + ${message}`);
  }
}

/**
 *
 * @type {Object[]}
 */
const commandsQueue = [];

function handleCommand(message) {
  console.log(message);
  if (message && message.type === 'command' && message.command) {
    const command = message.command;
    if (command.type === 'addShip') {
      const command = new AddSpaceShip({
        planets: [solar.planets[4], solar.planets[5]],
        type: 'spaceship',
        name: 'Fern & Fika',
        color:'purple'}
      )
      commandsQueue.push(command);
    }
  } else {
    throw new ClientError('Invalid payload');
  }
}

function createServer(port = 3001) {
  const app = express();
  app.use(cors());
  app.use(express.static('public/build'));
  const server = http.createServer(app);

  const appWs = expressWs(app, server);
  // console.log("AppWS", appWs);
  // app.get('/', (req, res) => {
  //   console.log("[GET]");
  //   res.send('Hello World!');
  // });

  app.ws('/', (
    /** @type {WebSocket} */ ws,
    req) => {

    const connection = {
      remoteAddress: req.connection.remoteAddress,
      remotePort: req.connection.remotePort,
      localAddress: req.connection.localAddress,
      localPort: req.connection.localPort,
    };
    mapWsToConnections.set(ws, connection);
    console.log('[WS][OPEN]', connection);
    webSockets.add(ws);

    ws.on('close', (message) => {
      console.log('[WS][CLOSE]', connection, message);
      webSockets.delete(ws);
      mapWsToConnections.delete(ws);
    });
    ws.on('message', (message) => {
      const payload = JSON.parse(message);
      console.log(`[WS][MSG]<${payload.message}>[TO]${webSockets.size}`);
      if (payload.type === 'command') {
        handleCommand(payload);
      } else {
        const response = JSON.stringify(
          {...payload, source: connection.remotePort}, null, 2);
        webSockets.forEach(ws => ws.send(response));
      }
    });
  });

  server.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
  });

  let counter = 0;

  function updateServer() {
    counter += 1;
    if (counter % 10000 === 0) {
      console.log('updateServer', counter, webSockets.size);
    }
    if (commandsQueue.length !== 0) {
      console.log(JSON.stringify({counter, commands: commandsQueue}, null, 2));
      solar.runCommands(commandsQueue);
      commandsQueue.length = 0;
    }


    solar.update();

    const transformations = Promise.all([
      solar.getVelocities().array(),
      solar.getPositions().array(),
      solar.getAccelerations().array(),
    ]);
    transformations.then((datas) => {
      const status = {velocities: datas[0], positions: datas[1], planets: solar.getPlanets(), accelerations: datas[2]};
      const update = {type: 'update', source: 'server', counter, data: status};
      webSockets.forEach(ws => ws.send(JSON.stringify(update)));
    });
  }

  setInterval(updateServer, 16);

  function connectionStatus() {
    console.log('connectionStatus', counter, webSockets.size);
 //   let index = 0;
 //   webSockets.forEach((ws) => console.log(index++, mapWsToConnections.get(ws), ws.readyState));
  }

  setInterval(connectionStatus, 100000);
}

export default createServer;
