import {unique} from '../../server/unique.js';
import {rColor} from '../physics/physics.js';
import {Vector3} from 'three';

/**
 * @typedef {object} CelestialBody
 * @property {string} name
 * @property {string} type
 * @property {number} fuel
 * @property {number[]} v
 * @property {number[]} x
 * @property {number} m
 * @property {number} r
 */

class AddSpaceShip {
  /**
   *
   * @type {CelestialBody}
   */
  celestialBody = {
    type: undefined,
    name: undefined,
    color: undefined,
    m: 0.000_000_000_000_1,
    r: 0.000_000_000_000_1,
    x: [0, 0, 0],
    v: [0, 0, 1],
    fuel: 1000
  };
  planet2;
  planet1;

  /**
   *
   * @param {Planet[2]} planets
   * @param {string} type
   * @param {string} name
   * @param {string} color
   */
  constructor({planets: [planet1, planet2], type = 'spaceship', name = unique(), color = rColor()}) {
    this.celestialBody.color = color;
    // todo ffi
    this.celestialBody.type = 'spaceship';
    this.celestialBody.name = name;
    this.planet1 = planet1;
    this.planet2 = planet2;
    this.celestialBody.x = this.makePosition();
    this.celestialBody.v = this.makeVelocity();
  }

  makePosition() {
    const v1 = new Vector3().fromArray(this.planet1.x);
    const v2 = new Vector3().fromArray(this.planet2.x);
    const length = (v1.length() + v2.length()) / 2;
    return [0, length, 0];
  }

  makeVelocity() {
    const v1 = new Vector3().fromArray(this.planet1.v);
    const v2 = new Vector3().fromArray(this.planet2.v);
    const length = (v1.length() + v2.length()) / 2;
    return [length, 0, 0];
  }
  //
  // /**
  //  *
  //  * @param {CelestialBody[]} planets
  //  * @param {Tensor[]} positions
  //  * @param {Tensor[]} velocities
  //  */
  /**
   *
   * @param {SolarUpdate} solarUpdate
   */
  run(solarUpdate) {

    // Ship mass is 1.2 times the fuel at start;
    // const mass = 1.2 * KG_MASS_CONVERSION * (1000 * spaceShipForm.fuel);

    solarUpdate.addCelestialBody(this.celestialBody, this.celestialBody.x, this.celestialBody.v);

  }

}
export default AddSpaceShip;
