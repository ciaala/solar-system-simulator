import * as physics from './physics/physics.js';
import {calculateNextAccelerations} from './physics/physics.js';
import * as tf from '@tensorflow/tfjs';
import {appendToTensor} from './tensorFlow/tensorUtils.js';

/**
 *
 */

class Solar {

  /**
   * @type {CelestialBody[]}
   */
  planets = []

  /**
   *
   * @type {Tensor}
   */
  currentPositions = undefined;
  /**
   *
   * @type {Tensor}
   */
  currentVelocities = undefined;
  /**
   *
   * @type {Tensor}
   */
  currentAccelerations = undefined;
  /**
   *
   * @type {number[]}
   */
  masses = physics.masses;

  /**
   * @type {number[]}
   */
  execution = [];

  static dt = tf.scalar(0.15);

  /**
   *
   * @param {CelestialBody[]} planets
   * @param {Tensor} initialPositionTF
   * @param {Tensor} initialVelocityTF
   */
  constructor({planets, initialVelocityTF, initialPositionTF}) {
    this.planets = planets;
    this.currentVelocities = initialVelocityTF
    this.currentPositions = initialPositionTF
    this.currentAccelerations = undefined;
  }

  /**
   *
   * @param {Scalar} deltaTimeTensor
   * @return {{newVelocity: *, newPosition: *, newAcceleration: Tensor}}
   */
  updatePositions(deltaTimeTensor) {
    const {newPositionTf, newVelocityTf, newAccelerationTf, masses} = tf.tidy(() => {
      const [accelerations, newMasses] = calculateNextAccelerations(
        this.planets,
        this.currentPositions,
        this.masses.length,
        this.masses,
      );
      return {
        newVelocityTf : this.currentVelocities.add(tf.mul(accelerations, deltaTimeTensor)),
        newPositionTf : this.currentPositions
          .add(tf.mul(this.currentVelocities, deltaTimeTensor))
          .add(tf.mul(accelerations, tf.pow(deltaTimeTensor,2))),
        masses: newMasses,
        newAccelerationTf : accelerations,
      };

    });
    tf.dispose([this.currentVelocities, this.currentPositions, this.currentAccelerations]);

    this.currentPositions = newPositionTf;
    this.currentVelocities = newVelocityTf;
    this.currentAccelerations = newAccelerationTf;
    this.masses = masses;
  }

  update() {
    const start = process.hrtime();

    this.updatePositions(Solar.dt);

    const end = process.hrtime(start);
    const currentInMs = end[0] * 10000 + Math.floor(end[1] / 1_000_000);
    this.execution.push(currentInMs);
    if (this.execution.length >= 10000) {

      const average = this.execution.reduce((v, c) => v + c, 0) / this.execution.length;
      const worst = this.execution.reduce((v, c) => v > c ? v : c, 0);
      console.info(`execution of ${this.execution.length}: average ${average} ms, worst ${worst} ms`);
      this.execution.length = 0;
    }

  }

  getPlanets() {
    return this.planets;
  }

  getPositions() {
    return this.currentPositions;
  }

  getVelocities() {
    return this.currentVelocities;
  }

  getAccelerations() {
    return this.currentAccelerations;
  }

  /**
   *
   * @param {AddShipCommand[]} commands
   */
  runCommands(commands) {

    /** @type {SolarUpdate}*/
    const solarUpdate = new SolarUpdate();
    commands.forEach(command => command.run(solarUpdate));
    if (solarUpdate.newBodies.length > 0) {
      const oldPositionTf = this.currentPositions;
      const oldVelocityTf = this.currentVelocities
      const oldAccelerations = this.currentAccelerations
      this.planets.push(...  solarUpdate.newBodies);
      this.masses.push(... solarUpdate.newBodies.map(body => body.m));
      this.currentPositions = appendToTensor(this.currentPositions, ...solarUpdate.newPositions);
      this.currentVelocities = appendToTensor(this.currentVelocities, ...solarUpdate.newVelocities);
      this.currentAccelerations = appendToTensor(this.currentAccelerations, ... solarUpdate.newAccelerations)
      tf.dispose([oldPositionTf, oldVelocityTf, oldAccelerations]);
    }

  }
}

class SolarUpdate {
  /**
   *
   * @type {CelestialBody[]}
   */
  newBodies = [];
  /**
   *
   * @type {number[][]}
   */
  newPositions = [];
  /**
   *
   * @type {number[][]}
   */
  newVelocities = [];
  /**
   *
   * @type {number[][]}
   */
  newAccelerations= [];
  /**
   *
   * @param {CelestialBody} celestialBody
   * @param {number[]} position
   * @param {number[]} velocity
   */
  addCelestialBody(celestialBody, position, velocity) {
    console.log(celestialBody);
    this.newBodies.push(celestialBody);
    this.newPositions.push(position);
    this.newVelocities.push(velocity);
    this.newAccelerations.push([0,0,0]);
  }
}

/**
 *
 * @return {Solar}
 */
function makeDefaultSolar() {
  return new Solar (physics);
}
export default Solar;
export {makeDefaultSolar};
