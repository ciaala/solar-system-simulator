import * as tf from '@tensorflow/tfjs';
/**
 * @param {Tensor} tensor
 * @param {number[]} newElements
 * @return {Tensor}
 */
function appendToTensor(tensor, ... newElements) {
  /**
   * @type {ArrayMap[R]}
   */
  const p = tensor.arraySync();
  const length = p.length + newElements.length;
  const extendedP = p.concat(newElements);
  return tf.tensor2d(extendedP, [length, 3]);
}

export {appendToTensor}
